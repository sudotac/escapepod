/*
 * PlayerService.kt
 * Implements the PlayerService class
 * PlayerService is Escapepod's foreground service that plays podcast audio
 *
 * This file is part of
 * ESCAPEPOD - Free and Open Podcast App
 *
 * Copyright (c) 2018-22 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */

package org.y20k.escapepod

import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.media.audiofx.AudioEffect
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.os.Looper
import androidx.media3.common.AudioAttributes
import androidx.media3.common.ForwardingPlayer
import androidx.media3.common.MediaItem
import androidx.media3.common.Player
import androidx.media3.exoplayer.ExoPlayer
import androidx.media3.exoplayer.analytics.AnalyticsListener
import androidx.media3.session.*
import com.google.common.collect.ImmutableList
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.y20k.escapepod.database.CollectionDatabase
import org.y20k.escapepod.database.objects.Episode
import org.y20k.escapepod.helpers.CollectionHelper
import org.y20k.escapepod.helpers.LogHelper
import org.y20k.escapepod.helpers.NotificationHelper
import org.y20k.escapepod.helpers.PreferencesHelper


/*
 * PlayerService class
 */
class PlayerService: MediaSessionService() {

    /* Define log tag */
    private val TAG: String = LogHelper.makeLogTag(PlayerService::class.java)


    /* Main class variables */
    private lateinit var player: Player
    private lateinit var mediaSession: MediaSession
    private lateinit var collectionDatabase: CollectionDatabase
    private lateinit var sleepTimer: CountDownTimer
    private val handler: Handler = Handler(Looper.getMainLooper())
    var sleepTimerTimeRemaining: Long = 0L
    var upNextEpisodeMediaId: String = String()


    /* Overrides onCreate from Service */
    override fun onCreate() {
        super.onCreate()
        // initialize player and session
        initializePlayer()
        initializeSession()
        setMediaNotificationProvider(CustomNotificationProvider())
        // get instance of database
        collectionDatabase = CollectionDatabase.getInstance(application)
        // get media id of the episode in Up Next queue
        upNextEpisodeMediaId = PreferencesHelper.loadUpNextMediaId()
    }


    /* Overrides onDestroy from Service */
    override fun onDestroy() {
        // player.removeAnalyticsListener(analyticsListener)
        handler.removeCallbacks(periodicProgressUpdateRunnable)
        player.removeListener(playerListener)
        player.release()
        mediaSession.release()
        super.onDestroy()
    }


    /* Overrides onGetSession from MediaSessionService */
    override fun onGetSession(controllerInfo: MediaSession.ControllerInfo): MediaSession {
        return mediaSession
    }


    /* Initializes the ExoPlayer */
    private fun initializePlayer() {
        val exoPlayer: ExoPlayer = ExoPlayer.Builder(this).apply {
            setAudioAttributes(AudioAttributes.DEFAULT, /* handleAudioFocus= */ true)
            setHandleAudioBecomingNoisy(true)
            setSeekBackIncrementMs(Keys.SKIP_BACK_TIME_SPAN)
            setSeekForwardIncrementMs(Keys.SKIP_FORWARD_TIME_SPAN)
        }.build()
        // exoPlayer.addAnalyticsListener(analyticsListener)
        exoPlayer.addListener(playerListener)

        // manually add seek to next and seek to previous since headphones issue them and they are translated to skip 30 sec forward / 10 sec back
        player = object : ForwardingPlayer(exoPlayer) {
            override fun getAvailableCommands(): Player.Commands {
                return super.getAvailableCommands().buildUpon().add(Player.COMMAND_SEEK_TO_NEXT).add(Player.COMMAND_SEEK_TO_PREVIOUS).build()
            }
        }
    }


    /* Initializes the MediaSession */
    private fun initializeSession() {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent = TaskStackBuilder.create(this).run {
                addNextIntent(intent)
                getPendingIntent(0, PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)
        }

        mediaSession = MediaSession.Builder(this, player).apply {
            setSessionActivity(pendingIntent)
            setCallback(CustomSessionCallback())
        }.build()
    }


    /* Starts sleep timer / adds default duration to running sleeptimer */
    private fun startSleepTimer() {
        // stop running timer
        if (sleepTimerTimeRemaining > 0L && this::sleepTimer.isInitialized) {
            sleepTimer.cancel()
        }
        // initialize timer
        sleepTimer = object: CountDownTimer(Keys.SLEEP_TIMER_DURATION + sleepTimerTimeRemaining, Keys.SLEEP_TIMER_INTERVAL) {
            override fun onFinish() {
                LogHelper.v(TAG, "Sleep timer finished. Sweet dreams.")
                sleepTimerTimeRemaining = 0L
                player.pause()
            }
            override fun onTick(millisUntilFinished: Long) {
                sleepTimerTimeRemaining = millisUntilFinished
            }
        }
        // start timer
        sleepTimer.start()
        // store timer state
        PreferencesHelper.saveSleepTimerRunning(isRunning = true)
    }


    /* Cancels sleep timer */
    private fun cancelSleepTimer() {
        if (this::sleepTimer.isInitialized && sleepTimerTimeRemaining > 0L) {
            sleepTimerTimeRemaining = 0L
            sleepTimer.cancel()
        }
        // store timer state
        PreferencesHelper.saveSleepTimerRunning(isRunning = false)
    }


    /* Try to start episode from Up Next queue */
    private fun startUpNextEpisode() {
        // get previous and new media ids
        val previousMediaId: String = player.currentMediaItem?.mediaId ?: String()
        val newMediaId: String = upNextEpisodeMediaId
        // clear Up Next queue
        upNextEpisodeMediaId = String()
        PreferencesHelper.saveUpNextMediaId()
        CoroutineScope(IO).launch {
            // set previous episode playback state to paused
            collectionDatabase.episodeDao().setPlaybackState(previousMediaId, stateBoolean = 0)
            // get Up Next episode
            val upNextEpisode: Episode? = collectionDatabase.episodeDao().findByMediaId(newMediaId)
            withContext(Main) {
                if (upNextEpisode != null) {
                    val position: Long = if (upNextEpisode.isFinished()) 0L else upNextEpisode.playbackPosition
                    player.setMediaItem(CollectionHelper.buildMediaItem(upNextEpisode, streaming = false), position)
                    player.prepare()
                } else {
                    player.stop()
                }
            }
        }
    }


    /*
     * Runnable: Periodically saves playback position
     */
    private val periodicProgressUpdateRunnable: Runnable = object : Runnable {
        override fun run() {
            val mediaId: String = player.currentMediaItem?.mediaId ?: String()
            val position: Long =  player.currentPosition
            val isPlaying: Boolean = player.isPlaying
            CoroutineScope(IO).launch {
                collectionDatabase.episodeDao().updatePlaybackPosition(mediaId, position, isPlaying)
            }
            // use the handler to start runnable again after specified delay
            handler.postDelayed(this, 15000)
        }
    }
    /*
     * End of declaration
     */


    /*
     * Custom MediaSession Callback that handles player commands
     */
    private inner class CustomSessionCallback: MediaSession.Callback {

        override fun onAddMediaItems(mediaSession: MediaSession, controller: MediaSession.ControllerInfo, mediaItems: MutableList<MediaItem>): ListenableFuture<List<MediaItem>> {
            val updatedMediaItems = mediaItems.map { mediaItem ->
                mediaItem.buildUpon().apply {
                    setUri(mediaItem.requestMetadata.mediaUri)
                }.build()
            }
            return Futures.immediateFuture(updatedMediaItems)
        }

        override fun onConnect(session: MediaSession, controller: MediaSession.ControllerInfo): MediaSession.ConnectionResult {
            // add custom commands
            val connectionResult: MediaSession.ConnectionResult  = super.onConnect(session, controller)
            val builder: SessionCommands.Builder = connectionResult.availableSessionCommands.buildUpon()
            builder.add(SessionCommand(Keys.CMD_START_SLEEP_TIMER, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_CANCEL_SLEEP_TIMER, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_REQUEST_SLEEP_TIMER_REMAINING, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_UPDATE_UP_NEXT_EPISODE, Bundle.EMPTY))
            builder.add(SessionCommand(Keys.CMD_START_UP_NEXT_EPISODE, Bundle.EMPTY))
            return MediaSession.ConnectionResult.accept(builder.build(), connectionResult.availablePlayerCommands);
        }

        override fun onCustomCommand(session: MediaSession, controller: MediaSession.ControllerInfo, customCommand: SessionCommand, args: Bundle): ListenableFuture<SessionResult> {
            when (customCommand.customAction) {
                Keys.CMD_START_SLEEP_TIMER -> {
                    startSleepTimer()
                }
                Keys.CMD_CANCEL_SLEEP_TIMER -> {
                    cancelSleepTimer()
                }
                Keys.CMD_REQUEST_SLEEP_TIMER_REMAINING -> {
                    val resultBundle = Bundle()
                    resultBundle.putLong(Keys.EXTRA_SLEEP_TIMER_REMAINING, sleepTimerTimeRemaining)
                    return Futures.immediateFuture(SessionResult(SessionResult.RESULT_SUCCESS, resultBundle))
                }
                Keys.CMD_START_UP_NEXT_EPISODE -> {
                    if (upNextEpisodeMediaId.isNotEmpty()) {
                        startUpNextEpisode()
                    }
                }
                Keys.CMD_UPDATE_UP_NEXT_EPISODE -> {
                    upNextEpisodeMediaId = args.getString(Keys.EXTRA_UP_NEXT_EPISODE_MEDIA_ID) ?: String()
                    PreferencesHelper.saveUpNextMediaId(upNextEpisodeMediaId)
                }
            }
            return super.onCustomCommand(session, controller, customCommand, args)
        }

        override fun onPlayerCommandRequest(session: MediaSession, controller: MediaSession.ControllerInfo, playerCommand: Int): Int {
            // playerCommand = one of COMMAND_PLAY_PAUSE, COMMAND_PREPARE, COMMAND_STOP, COMMAND_SEEK_TO_DEFAULT_POSITION, COMMAND_SEEK_IN_CURRENT_MEDIA_ITEM, COMMAND_SEEK_TO_PREVIOUS_MEDIA_ITEM, COMMAND_SEEK_TO_PREVIOUS, COMMAND_SEEK_TO_NEXT_MEDIA_ITEM, COMMAND_SEEK_TO_NEXT, COMMAND_SEEK_TO_MEDIA_ITEM, COMMAND_SEEK_BACK, COMMAND_SEEK_FORWARD, COMMAND_SET_SPEED_AND_PITCH, COMMAND_SET_SHUFFLE_MODE, COMMAND_SET_REPEAT_MODE, COMMAND_GET_CURRENT_MEDIA_ITEM, COMMAND_GET_TIMELINE, COMMAND_GET_MEDIA_ITEMS_METADATA, COMMAND_SET_MEDIA_ITEMS_METADATA, COMMAND_CHANGE_MEDIA_ITEMS, COMMAND_GET_AUDIO_ATTRIBUTES, COMMAND_GET_VOLUME, COMMAND_GET_DEVICE_VOLUME, COMMAND_SET_VOLUME, COMMAND_SET_DEVICE_VOLUME, COMMAND_ADJUST_DEVICE_VOLUME, COMMAND_SET_VIDEO_SURFACE, COMMAND_GET_TEXT, COMMAND_SET_TRACK_SELECTION_PARAMETERS or COMMAND_GET_TRACK_INFOS. */
            // emulate headphone buttons
            // start/pause: adb shell input keyevent 85
            // next: adb shell input keyevent 87
            // prev: adb shell input keyevent 88
            when (playerCommand) {
                Player.COMMAND_SEEK_TO_NEXT ->  {
                    // override "skip to next" command - just skip forward instead
                    player.seekForward()
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                Player.COMMAND_SEEK_TO_PREVIOUS ->  {
                    // override "skip to previous" command - just skip back instead
                    player.seekBack()
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                Player.COMMAND_STOP -> {
                    // todo hide notification
                    player.pause()
//                    mediaSession.player.release()
//                    mediaSession.release()
//                    stopSelf()
//                    player.clearMediaItems()
//                    stopForeground(true)
                    return SessionResult.RESULT_INFO_SKIPPED
                }
                else -> {
                    return super.onPlayerCommandRequest(session, controller, playerCommand)
                }
            }
        }
    }
    /*
     * End of inner class
     */


    /*
     * Custom NotificationProvider that sets tailored Notification
     */
    private inner class CustomNotificationProvider: MediaNotification.Provider {

        override fun createNotification(session: MediaSession, customLayout: ImmutableList<CommandButton>, actionFactory: MediaNotification.ActionFactory,  onNotificationChangedCallback: MediaNotification.Provider.Callback): MediaNotification {
            return MediaNotification(Keys.NOW_PLAYING_NOTIFICATION_ID, NotificationHelper(this@PlayerService).getNotification(session, actionFactory))
        }

        override fun handleCustomCommand(session: MediaSession, action: String, extras: Bundle): Boolean {
            TODO("Not yet implemented")
        }
    }
    /*
     * End of inner class
     */

    /*
     * Player.Listener: Called when one or more player states changed.
     */
    private val playerListener: Player.Listener = object : Player.Listener {

        override fun onIsPlayingChanged(isPlaying: Boolean) {
            super.onIsPlayingChanged(isPlaying)
            val mediaId: String = player.currentMediaItem?.mediaId ?: String()
            val currentPosition: Long = player.currentPosition
            // store state of playback
            PreferencesHelper.saveIsPlaying(isPlaying)
            PreferencesHelper.saveCurrentMediaId(mediaId)
            if (mediaId == upNextEpisodeMediaId) PreferencesHelper.saveUpNextMediaId()
            // save playback state of episode to database
            CoroutineScope(IO).launch {
                collectionDatabase.episodeDao().updatePlaybackPosition(mediaId, currentPosition, isPlaying)
            }

            if (isPlaying) {
                // playback is active - start to periodically save the playback position to database
                handler.removeCallbacks(periodicProgressUpdateRunnable)
                handler.postDelayed(periodicProgressUpdateRunnable, 0)

            } else {
                // playback is not active - stop periodically saving the playback position to database
                handler.removeCallbacks(periodicProgressUpdateRunnable)

                // cancel sleep timer
                cancelSleepTimer()

                // Not playing because playback is paused, ended, suppressed, or the player
                // is buffering, stopped or failed. Check player.getPlayWhenReady,
                // player.getPlaybackState, player.getPlaybackSuppressionReason and
                // player.getPlaybackError for details.
                when (player.playbackState) {
                    // player is able to immediately play from its current position
                    Player.STATE_READY -> {
                        // todo
                    }
                    // buffering - data needs to be loaded
                    Player.STATE_BUFFERING -> {
                        // todo
                    }
                    // player finished playing all media
                    Player.STATE_ENDED -> {
                        // try to start Up Next episode
                        if (upNextEpisodeMediaId.isNotEmpty()) {
                            startUpNextEpisode()
                        }
                    }
                    // initial state or player is stopped or playback failed
                    Player.STATE_IDLE -> {
                        // todo
                    }
                }
            }
        }
    }
    /*
     * End of declaration
     */


    /*
     * Custom AnalyticsListener that enables AudioFX equalizer integration
     */
    private val analyticsListener = object: AnalyticsListener {
        override fun onAudioSessionIdChanged(eventTime: AnalyticsListener.EventTime, audioSessionId: Int) {
            super.onAudioSessionIdChanged(eventTime, audioSessionId)
            // integrate with system equalizer (AudioFX)
            val intent: Intent = Intent(AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION)
            intent.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, audioSessionId)
            intent.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, packageName)
            sendBroadcast(intent)
        }
    }
    /*
     * End of declaration
     */

}