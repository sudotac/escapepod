# Assets

<img src="https://codeberg.org/y20k/escapepod/raw/branch/master/app/src/main/res/mipmap-xxxhdpi/ic_launcher_round.png" width="96" />

## Adaptive Icon

How to create Escapepod's [Adaptive Icon](https://developer.android.com/guide/practices/ui_guidelines/icon_design_adaptive) using Android Studio:

1. Go to `File > New > Image Asset`
2. Choose `Icon Type > Launcher Icons (Adaptive and Legacy)`
3. Foreground Layer: Select the file [escapepod-app-icon-current-foreground.png](https://codeberg.org/y20k/escapepod/raw/branch/master/assets/escapepod-app-icon-current-foreground.png)
4. Background Layer: Select the file [escapepod-app-icon-current-background.png](https://codeberg.org/y20k/escapepod/raw/branch/master/assets/escapepod-app-icon-current-background.png)
5. Foreground Layer: Set Trim to `No`
6. Foreground Layer: Set Resize to `100%`